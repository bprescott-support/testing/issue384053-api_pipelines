#!/bin/bash -x

for branch in target1 target2 target3 ; do

  curl --insecure -X POST -F "token=$CI_JOB_TOKEN" -F "ref=$branch" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/trigger/pipeline"

done
